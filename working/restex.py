from flask import Flask, request
from flask_restful import reqparse, abort, Api, Resource
from flask_cors import CORS, cross_origin
from atmega import *



app = Flask(__name__)
CORS(app, resources=r'/*') # to allow external resources to fetch data
api = Api(app)

status = 0

@api.resource("/")
class url_index( Resource ):
    def get( self ):
       returnMessage = {"message": "O.K" }
       return returnMessage
       
      
@api.resource("/relay_status/on")
class url_relay_status_on( Resource ):
    def get( self ):
       returnMessage = {"message": "ON" }
       send('on')
       return returnMessage

@api.resource("/relay_status/off")
class url_relay_status_off( Resource ):
    def get( self ):
       returnMessage = {"message": "OFF" }
       send('off')
       return returnMessage


            

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
