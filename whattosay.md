We have built a system with a relay, atmega328 and a raspberry pi. That will then connect to the internet and have a webserver controlling the relay with on and off functions.

The relayboard we are using has two relays on it, the connections are VCC, Ground and a toggle for each relay on the board. For controlling the lamp we’re using the “normally open” and then closing the circuit with the atmega.

The Atmega328 contains the code that sends signal to the relay when ever it receives on or off commands from the website dashboard. Communication between the atmega and the Rpi is going through UART communication.

Our Raspberry pi has a flask python script running that translates our HTTP requests from the webserver so the atmega can understand which commands it should run.

The webserver that hosts our dashboard is runnin nginx as the webhost program. The dashboard is a simple page containing the light control for turning the relay on and off. The HTTP log sections is used for debugging 

Now we have a fully functioning setup and we achieved our goal for this small project. Expansion of the system would be fairly easy, and adding the 2nd relay to the system by adding another section on the dashboard for on/off buttons and another section in the C. code.
